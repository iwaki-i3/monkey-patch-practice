require './active_hoge'
require './monkey_patch/hoge_ext'

class Main
  def initialize
    @active_hoge = ActiveHoge.new(1)
  end

  def run
    puts @active_hoge.inspect
    puts @active_hoge.inc
    puts @active_hoge.inc2
  end
end

class ActiveHogeHoge < ActiveHoge
  def inc3
    @base + @hoge + @hoge + @hoge
  end
end

class Main2
  using HogeExt

  def initialize
    @active_hoge = ActiveHogeHoge.new(10)
  end

  def run
    puts @active_hoge.inspect
    puts @active_hoge.inc
    puts @active_hoge.inc2
  end
end

Main.new.run

Main2.new.run
