module HogeExt
  refine ActiveHoge.singleton_class do
    def new(base)
      super.tap do |instance|
        instance.instance_variable_set(:@hoge_ext, 2)
      end
    end
  end
  refine ActiveHoge do
    def inc
      puts ">#{@hoge_ext}"
      super + @hoge_ext
    end
  end
end
