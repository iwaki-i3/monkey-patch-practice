class ActiveHoge
  def initialize(base)
    @base = base
    @hoge = 1
  end

  def inc
    @base + @hoge
  end

  def inc2
    @base + @hoge + @hoge
  end
end
